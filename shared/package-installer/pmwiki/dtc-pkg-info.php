<?php

$pkg_info = array(
  "name" => "PmWiki",
  "version" => "2.2.25",
  "short_desc" => "A wiki-based system for collaborative creation and maintenance of websites.",
  "long_desc" => "PmWiki pages look and act like normal web pages, except they have an \"Edit\" link that makes it easy to modify existing pages and add new pages into the website, using basic editing rules. You do not need to know or use any HTML or CSS. Page editing can be left open to the public or restricted to small groups of authors.",

  "unpack_disk_usage" => "1261438",
  "need_database" => "no",
  "sql_script" => "no",

  "onthefly_post_script" => "no",
  "post_script_url" => "",

  "remove_folder" => "no",
  "remove_folder_path" => array(),

  "need_admin_email" => "no",
  "need_admin_login" => "no",
  "need_admin_pass" => "no",

  "can_select_directory" => "yes",
  "directory" => "",

  "has_install_script" => "no",
  "install_script_url" => "",

  "unpack_type" => "tar.gz",
  "file" => "pmwiki-2.2.25.tgz",
  "resulting_dir" => "pmwiki-2.2.25",
  "renamedir_to" => "pmwiki");

?>
